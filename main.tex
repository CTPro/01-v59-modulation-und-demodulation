\input{header.tex}
\input{metadata.tex}
\input{shortcuts.tex}

\author{
  Tim Kallage    \\    tim.kallage@tu-dortmund.de \and
  Christian Geister \\ christian.geister@tu-dortmund.de
}
\title{(\Versuchsnummer)\\\Versuchstitel}
\date{Durchführung: \Versuchsdatum}

\begin{document}
\maketitle
\thispagestyle{empty}
\vfill
\tableofcontents
\newpage

%------------------------------------------------
\section{Motivation}
%------------------------------------------------
Um elektrische Signale zu übertragen wird dieses Signal auf eine Trägerwelle moduliert.
Dies hat den Grund, dass einerseits eine relativ große Bandbreite eines Signals auf einen
kleinen Bandbreitenbereich \enquote{komprimiert} werden kann, andererseits
können so Trägerwellen benutzt werden, die mit einer bestimmten Frequenz z.B. eine höhere Reichweite haben.
Am Zielort muss aus der modulierten Welle die Information durch Demodulation extrahiert werden.
In diesem Versuch werden verschiedene Methoden zur Modulation und Demodulation von elektrischen Signalen untersucht.
%------------------------------------------------
\section{Theoretische Grundlagen}
%------------------------------------------------
Die verschiedenen existierenden Modulationsverfahren lassen sich in mehrere Klassen einteilen.
Hier werden die Amplitudenmodulation und Frequenzmodulation betrachet.
Beide sollen kurz vorgestellt werden.
\FloatBarrier
\subsection{Amplitudenmodulation} % (fold)
\label{sub:amplitudenmodulation}
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/U_AM.PNG}
	\caption{Amplitudenmoduliertes Signal \cite{1}.}
	\label{fig:U_AM}
\end{figure}%
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.5\textwidth]{media/Spektrum_Amplmod.png}
	\caption{Frequenzspektrum einer Amplitudenmodulierten Schwingung nach \eqref{eq:U3b} \cite{1}.}
	\label{fig:Spektrum_Amplmod}
\end{figure}%
Bei dieser Art der Modulation wird die Amplitude einer hochfrequenten
Trägerwelle mit einem niederfrequenten Signal moduliert.
Es gilt:
\begin{align}
	U_\text{T}(t) &= \hat{U}_\text{T}\cos (\omegaT t) , \\
	U_\text{M}(t) &= \hat{U}_\text{M}\cos (\omegaM t) .
\end{align}
Die modulierte Welle soll dann folgende Gestalt besitzen:
\begin{equation} \label{eq:U3a}
	U_3 = \hat{U}_\text{T} (1 + m\cos(\omegaM t)) \cos(\omegaT t) ,
\end{equation}
mit dem Modulationsgrad
\begin{equation}
	m = \gamma \hat{U}_\text{M} .
\end{equation}
Dieser kann einen Wert zwischen 0 und 1 annehmen, wodurch die Amplitude der modulierten Welle im Bereich
\begin{equation}
	\hat{U}_\text{T} (1-m) \le U_3 \le \hat{U}_\text{T} (1+m)
\end{equation}
liegt.
\\
Durch Umformung von \ref{eq:U3a} ergibt sich
\begin{equation} \label{eq:U3b}
	U_3 = \hat{U}_\text{T}\left(\cos(\omegaT t)
		+ \frac{1}{2}m\cos((\omegaT + \omegaM)t)
		+ \frac{1}{2}m\cos((\omegaT - \omegaM)t)
		\right) .
\end{equation}
Man erkennt, dass das Frequenzspektrum dieser Welle die Trägerfrequenz, sowie zweimal die Modulationsfrequenz enthält, wie in Abb.~\ref{fig:Spektrum_Amplmod} dargestellt.
Die Seitenlinien verbreitern sich zu Seitenbändern, falls mehr als eine Frequenz übertragen wird, wie beispielsweise bei Sprachübertragung.
Die gesamte zu übertragende Information ist bereits in einem der Seitenbänder enthalten.
Bei der normalen Übertragung dieser Welle, ist der größte Teil der Energie in der Trägerwelle enthalten, da dieser Teil die größte
Amplitude besitzt, wie in Formel \eqref{eq:U3a} zu erkennen ist.
Diese überträgt allerdings keine Information, weshalb sie unterdrückt werden kann.
Dadurch kann die Energie eingespart werden, die zur Abstrahlung der Trägerwelle benötigt wird.
Außerdem kann durch die Unterdrückung eines der Seitenbänder die verwendete Bandbreite verringert werden.
Diese Technik wird dann Einseitenbandmodulation genannt.
\\
Amplitudenmodulierte Signale sind, im Vergleich zu frequenzmodulierten, störanfällig und verzerren leichter.
% subsection amplitudenmodulation (end)
\FloatBarrier
\subsection{Frequenzmodulation} % (fold)
\label{sub:frequenzmodulation}
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/U_FM.PNG}
	\caption{Frequenzmoduliertes Signal \cite{1}.}
	\label{fig:U_FM}
\end{figure}
Die Frequenzmodulation vermeidet die oben genannten Probleme der Amplitudenmodulation weitgehend.
Bei dieser Technik wird die Frequenz der Trägerwelle mit der Signalfrequenz moduliert.
Die Zeitabhängigkeit der resultierenden Welle ergibt sich zu
\begin{equation} \label{eq:U_FMa}
	U(t) = \hat{U}\sin\left( \omegaT t + m \frac{\omegaT}{\omegaM} \cos (\omegaM t) \right) .
\end{equation}
Die Ableitung dieses Ausdrucks ergibt die Momentanfrequenz
\begin{equation}
	f = \frac{\omegaT}{2\pi} (1 - m \sin(\omegaM t)) ,
\end{equation}
mit der Variationsbreite der Schwingungsfrequenz $m\omegaT / 2 \pi$, dem Frequenzhub.
\\
Im Folgenden wird die Näherung für geringen Frequenzhub
\begin{equation}
	m\frac{\omegaT}{\omegaM}
\end{equation}
durchgeführt. Dieser Fall wird Schmalband-Frequenzmodulation genannt.
Nach kurzer Rechnung und entsprechender Näherung bekommt man aus \eqref{eq:U_FMa} folgenden Ausdruck:
\begin{equation} \label{eq:U_FMb}
	U(t) = \hat{U}\left\{ \sin(\omega_T t)
		+ \frac{1}{2}m\frac{\omegaT}{\omegaM}\cos((\omegaT + \omegaM)t)
		+ \frac{1}{2}m\frac{\omegaT}{\omegaM}\cos((\omegaT - \omegaM)t)
		+ \mathcal{O}\left(\left(m\frac{\omegaT}{\omegaM}\right)^2\right)
		\right\} .
\end{equation}
Dieser Ausdruck besteht wieder aus drei Teilschwingungen. Im Unterschied zum amplitudenmodulierten Signal sind hier die Seitenlinien um $\frac{\pi}{2}$ in der Phase verschoben. Dies wird deutlich, wenn die Kosinus-Terme in Formel \eqref{eq:U_FMb} als $\sin\left((\omegaT-\omegaM)t + \frac{\pi}{2}\right)$ geschrieben werden.
% subsection frequenzmodulation (end)
\FloatBarrier
\subsection{Modulationsschaltungen} % (fold)
\label{sub:modulationsschaltungen}
\subsubsection*{Amplitudenmodulator}
Zur Erzeugung einer amplitudenmodulierten Schwingung benötigt man nach Gleichung \eqref{eq:U3a} ein Bauelement, welches in der Lage ist, zwei Spannungen miteinander zu multiplizieren.
Dazu ist im Prinzip jedes Bauelement mit einer nicht-linearen Kennlinie geeignet, beispielsweise eine Diode.
Die Potenzreihenentwicklung der Diode ist
\begin{equation}
	I(U) = a_0 + a_1 U + a_2 U^2 + \dots\quad.
\end{equation}
Für $U = U_\text{T} + U_\text{M}$ also
\begin{equation}
	I(U_\text{T} + U_\text{M}) = a_0 + a_1 (U_\text{T} + U_\text{M}) + a_2 (U_\text{T}^2 + U_\text{M}^2) + 2 a_2 U_\text{T} U_\text{M} + \dots\quad.
\end{equation}
Die unerwünschten Terme $U_\text{M}, U_\text{M}^2, U_\text{T}^2$ können durch eine geschickte Wahl der Bauelemente und deren Anordnung unterdrückt werden.
\begin{figure}
	\centering
	\includegraphics[width=0.8\textwidth]{media/Ringmodulator.png}
	\caption{Schaltungsbild eines Ringmodulators \cite{1}.}
	\label{fig:Ringmodulator}
\end{figure}%
Eine geeignete Schaltung ist der Ringmodulator, der in Abb.~\ref{fig:Ringmodulator} dargestellt ist.
Beim Anlegen einer Modulationsfrequenz variieren die Teilungsverhältnisse in den Zweigen A, B und C, D im Rhytmus von $U_\text{M}$.
Bei idealen Verhältnissen ist die Ausgangsspannung gegeben durch
\begin{equation} \label{eq:U_Ra}
	U_\text{R}(t) = \gamma U_\text{M}(t) U_\text{T}(t) .
\end{equation}
Für
\begin{equation}
	U_\text{T}(t) = \hat{U}_\text{T}\cos(\omegaT t)\quad\text{und}\quad
	U_\text{M}(t) = \hat{U}_\text{M}\cos(\omegaT t + \varphi)
\end{equation}
ergibt sich \eqref{eq:U_Ra} zu
\begin{equation} \label{eq:U_Rb}
	U_\text{R}(t) = \gamma\hat{U}_\text{T}\hat{U}_\text{M}\frac{1}{2}\cos((\omegaT + \omegaM)t + \varphi)
		+ \gamma\hat{U}_\text{T}\hat{U}_\text{M}\frac{1}{2}\cos((\omegaT - \omegaM)t - \varphi) ,
\end{equation}
es treten also nur die beiden Seitenlinien auf.
\FloatBarrier
\subsubsection*{Frequenzmoduliert}
\begin{figure}
	\centering
	\includegraphics[width=.8\textwidth]{media/Frequenzmodulator.PNG}
	\caption{Schaltbild des beschriebenen Frequenzmodulators (für geringen Frequenzhub) \cite{1}.}
	\label{fig:Frequenzmodulator}
\end{figure}%
Mit etwas Modifikation kann der Ringmodulator auch für die Erzeugung von frequenzmodulierten Signalen mit geringem Frequenzhub verwendet werden.
Aus dem Vergleich von \eqref{eq:U_FMb} mit \eqref{eq:U_Rb} ist erkennbar, dass die um $\frac{\pi}{2}$ verschobene Trägerwelle zum Ausgangssignal addiert werden kann, um eine frequenzmodulierte Schwingung zu erzeugen.
Eine Schaltung, die dies ausnutzt ist in Abb.~\ref{fig:Frequenzmodulator} dargestellt.
% subsection modulationsschaltungen (end)
\FloatBarrier
\subsection{Demodulationsschaltungen} % (fold)
\label{sub:demodulationsschaltungen}
\subsubsection*{Amplitudenmodulator}
\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{media/Demod_Ringmodulator.PNG}
	\caption{Demodulator-Schaltung mit einem Ringmodulator \cite{1}.}
	\label{fig:Demod_Ringmodulator}
\end{figure}%
Ein Ringmodulator kann auch zur Demodulation eines Signals verwendet werden.
Dazu wird auf den Eingang $R$ das modulierte Signal gegeben, sowie auf $L$ eine Schwingung mit der Frequenz $\omegaT$, siehe Abb.~\ref{fig:Demod_Ringmodulator}.
Am Ausgang $X$ entsteht dann eine Spannung, die die Frequenzen $\omegaM$, $2\omegaT - \omegaM$ und $2\omegaT + \omegaM$ enthält, sowie bei Trägerabstrahlung ebenfalls eine Gleichspannung.
Diese ist proportional zum Kosinus der Phasenverschiebung zwischen der Trägerfrequenz und der Frequenz auf dem Eingang $L$.
In der Praxis ist es oft ein Problem eine konstante Phase zwischen der abgestrahlten Trägerfrequenz und der Frequenz zur Demodulation zu erhalten.
Dem wird durch die Nutzung von sogenannten Phasenregelkreisen entgegengewirkt.
\\
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{media/Demod_Diode.PNG}
	\caption{Demodulatorschaltung mit Diode und Tiefpass \cite{1}.}
	\label{fig:Demod_Diode}
\end{figure}%
Alternativ wird eine andere Demodulator-Schaltung verwendet, bei der dieses Problem nicht auftritt, bespielsweise bestehend aus einer Diode und einem Tiefpass, wie in Abb.~\ref{fig:Demod_Diode} dargestellt.
Der Tiefpassfilter dient hier dazu, die hochfrequenten Anteile mit den Frequenzen $2^n\cot \omegaT$ aus den Halbwellen zu entfernen.
In der Näherung für einen kleinen Modulationgrad kann die Kennlinie einer Diode linear angenommen werden, was dazu führt, dass nach der beschriebenen Demodulationsschaltung nur das gewünschte Signal mit der Frequenz $\omegaM$ übrig bleibt.
Bei größeren Modulationsgraden entstehen durch die exponetielle Kennlinie der Diode Verzerrungen.
\FloatBarrier
\subsubsection*{Frequenzmoduliert}
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{media/Demod_Freqmod.PNG}
	\caption{Schaltung eines Flankenmodulators \cite{1}.}
	\label{fig:Demod_Freqmod}
\end{figure}%
Zur Demodulation einer frequenzmodulierten Spannung lässt sich ein sogenannter Flankenmodulator verwenden, der in Abb.~\ref{fig:Demod_Freqmod} dargestellt ist.
Bei dieser Konfiguration wird die Frequenzabhängigkeit der Kondensatorspannung bei erzwungenen Schwingungen ausgenutzt.
\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{media/Resonanzkurve.PNG}
	\caption{Die Resonanzkurve eines Schwinkreises in einem Flankenmodulator \cite{1}.}
	\label{fig:Resonanzkurve}
\end{figure}%
Durch die geeignete Einstellung der Resonanzfrequenz, sodass die Trägerfrequenz in der steilen Flanke der Resonanzüberhöhung liegt, verursacht eine kleine Variation in der Frequenz eine vergleichsweise große Variation in der Kondensatorspannungsamplitude.
So wird das frequenzmodulierte Signal in ein amplitudenmoduliertes gewandelt.
Ein weitgehend verzerrungsfreies, amplitudenmoduliertes Signal erhält man hierbei wieder bei geringem Frequenzhub, sodass der gesamte Frequenzbereich im linearen Teil der Resonanzkurve liegt.
% subsection demodulationsschaltungen (end)
\FloatBarrier


%------------------------------------------------
\section{Aufbau und Durchführung}
%------------------------------------------------
\subsection{Amplitudenmodulation mit Trägerunterdrückung}
Für die Amplitudenmodulation wird ein Ringmodulator verwendet.
Mit Hilfe eines Frequenzgenerators wird ein Trägersignal mit einer Frequenz von  \SI{10}{\mega\hertz} erzeugt, die auf den Eingang für die Trägerspannung des Ringmodulators gegeben wird. 
Der Modulationseingang wird mit einer Spannung von \SI{1}{\kilo\hertz} belegt. 
Am Ausgang $R$ erhält man ein amplitudenmoduliertes Signal mit Trägerunterdrückung, welches mit einem Oszilloskop nachgewiesen wird.

Weiterhin wird mit dem Digitaloszilloskop das Frequenzspektrum der modulierten Spannung analysiert.
Bei einer Abtastrate von 2 GSa/s sind Frequenzen bis zu einigen Vielfachen der Trägerfrequenz messbar. Mit einem Spektrumanalysator können Frequenzen bis zu \SI{1.5}{\giga\hertz} gemessen werden. Die Bandbreite des Oszilloskops ist jedoch ausreichend. Gerade bei höheren Frequenzen wie der hier auftretenden Trägerspannung liefert der Spektrumanalysator eine bessere Spektrale Auflösung. Für die gewählte Modulation und Trägerspannung ist eine qualitative Analyse auch mit der Frequenzanalyse des Oszilloskops möglich.

\subsection{Amplitudenmodulation mit Trägerabstrahlung}
Um ein amplitudenmoduliertes Signal mit Trägerabstrahlung zu erhalten, wird eine Schaltung nach Abbildung \ref{fig:traegerabstrahlung} aufgebaut. Es wird ebenfalls die modulierte Spannung und das Frequenzspektrum untersucht.
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Trägerabstrahlung.PNG}
	\caption{Schaltung zur Amplitudenmodulation mit Trägerabstrahlung \cite{1}.}
	\label{fig:traegerabstrahlung}
\end{figure}

\subsection{Frequenzmodulation}
Um eine frequenzmodulierte Spannung zu erzeugen wird eine Schaltung nach Abbildung \ref{fig:Frequenzmodulator} aufgebaut. Der Oszillograph wird mit Hilfe der Trägerspannung getriggert. So ist es möglich die Phasenvariation zu beobachten.

\subsection{Demodulation einer amplitudenmodulierten Spannung}
Zunächst wird gezeigt, dass mit einer Schaltung nach Abbildung \ref{fig:gleichrichter} die Ausgangsspannung $X$ an einem Ringmodulator proportional zur Phase zwischen den Eingangsspannungen ist. Damit lässt sich ein Ringmodulator auch zur Demodulation verwenden. Die Trägerspannung muss jedoch zur Verfügung stehen.

Die demodulierte Spannung wird mit der ursprünglichen Spannung auf einem Oszilloskop verglichen. Dazu wird die Schaltung in Abbildung \ref{fig:Demod_Ringmodulator} aufgebaut.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Gleichrichter.PNG}
	\caption{Ringmodulator als phasenempfindlicher Gleichrichter \cite{1}.}
	\label{fig:gleichrichter}
\end{figure}

\subsection{Demodulation mit einer Gleichrichterdiode}
Eine amplitudenmodulierte Spannung lässt sich auch mit einer Gleichrichterdiode demodulieren. Dazu wird die Schaltung aus Abbildung \ref{fig:Demod_Diode} verwendet, wobei die Spannung am Punkt $A$ von Interesse ist.

\subsection{Demodulation einer frequenzmodulierten Spannung}
Mit Hilfe eines Schwingkreises kann das frequenzmodulierte Signal in ein amplitudenmoduliertes umgewandelt werden. Dazu wird die Schaltung nach Abbildung \ref{fig:Demod_Freqmod} verwendet. Nach einem Tiefpass sollte die Modulationsspannung wieder zu erkennen sein.



%------------------------------------------------
\section{Auswertung}
%------------------------------------------------
\subsection{Amplitudenmodulation mit Trägerunterdrückung}
Mit einer Trägerspannung von \SI{10}{\mega\hertz} und einer Modulationsspannung von \SI{1}{\kilo\hertz} erhält man die Schwebung in Abbildung \ref{plt:teil_a}
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_a.PNG}
	\caption{Amplitudenmoduliertes Signal.}
	\label{plt:teil_a}
\end{figure}
Die Frequenzanalyse des Oszilloskops ergibt Abbildung \ref{plt:teil_b_fft}. Hieraus erkennt man zwei Peaks um \SI{10}{\mega\hertz}, die den Frequenzbändern $\omega_T \pm \omega_M$ entsprechen.
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_b_FFT.PNG}
	\caption{Frequenzspektrum des amplitudenmodulierten Signals aus Abbildung \ref{plt:teil_a}.}
	\label{plt:teil_b_fft}
\end{figure}

\subsection{Amplitudenmodulation mit Trägerabstrahlung}
Das amplitudenmodulierte Signal mit Trägerabstrahlung ist in Abbildung \ref{plt:teil_c} dargestellt. Daraus ist ein Modulationsgrad von $m=\SI{0.16+-0.01}{}$ zu entnehmen, indem man die Amplitude der Modulation bestimmt und diese durch die Amplitude der Trägerspannung teilt. Die Frequenzanalyse ergibt Abbildung \ref{plt:teil_c_fft}. Hier erkennt man neben den Peaks um \SI{10}{\mega\hertz} auch einen genau bei \SI{10}{\mega\hertz}, der der Trägerfrequenz entspricht. Bei den Vielfachen der Trägerfrequenz ist ein ähnliches Muster zu erkennen, jedoch mit geringerer Amplitude. Diese Oberwellen entstehen durch die Nichtlinearität der Dioden im Ringmodulator.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_c.PNG}
	\caption{Amplitudenmoduliertes Signal mit Trägerabstrahlung.}
	\label{plt:teil_c}
\end{figure}
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_c_FFT.PNG}
	\caption{Frequenzspektrum des Signals aus Abbildung \ref{plt:teil_c}.}
	\label{plt:teil_c_fft}
\end{figure}

\subsection{Frequenzmodulation}
Die Periodendauer der Trägerspannung von \SI{100}{\nano\second} wird mit \SI{8}{\nano\second} variiert. Damit ergibt sich eine Frequenzvariation von \SI{870}{\kilo\hertz}. Teilt man die Variationsfrequenz durch die Trägerfrequenz erhält man einen Modulationsgrad von \SI{0.087}{}. 
Die Trägerspannung sowie das modulierte Signal sind in Abbildung \ref{plt:teil_d_breite} dargestellt.
Die Frequenzanalyse in Abbildung \ref{plt:teil_d_fft} zeigt eine Verbreiterung des Peaks bei \SI{10}{\mega\hertz} (und auch bei den Oberwellen). Durch die Frequenzmodulation wird die Frequenz variiert, dies entspricht der Verbreiterung des Peaks der Trägerfrequenz.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_d_Breite.PNG}
	\caption{Darstellung des Trägersignals in rot, sowie des frequenzmodulierten Signals in blau. Dabei wurde Nachleuchten verwendet um die Breite der Frequenzmodulation zu bestimmen.}
	\label{plt:teil_d_breite}
\end{figure}
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_d_FFT.PNG}
	\caption{Frequenzspektrum des frequenzmodulierten Signals.}
	\label{plt:teil_d_fft}
\end{figure}


\subsection{Demodulation einer amplitudenmodulierten Spannung}
In Abbildung \ref{plt:teil_e} ist die Ausgangsspannung an $X$ des Ringmodulators zum Kosinus der Phase dargestellt. Es wurde eine Gerade der Form $U=a \cos\varphi + b$ an die Messwerte gefittet. Als Ergebnis liefert die curve\_fit Funktion des Python Package scipy.optimze
\begin{align}
a =& \SI{108.8+-0.8}{\volt}\\
b =& \SI{10.0+-0.4}{\volt}.
\end{align}

Die Ausgangsspannung ist somit mit vernachlässigbarem Fehler im mittleren Bereich proportional zum Kosinus der Phase $\phi$.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{build/Teil_e.pdf}
	\caption{Abhängigkeit der Ausgangsspannung von der Phasendifferenz.}
	\label{plt:teil_e}
\end{figure}

Die Demodulation mit einem Ringmodulator ergibt eine Kurve, die in Abbildung \ref{plt:teil_f} dargestellt ist.
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_f.PNG}
	\caption{Demodulation mit einem Ringmodulator. Demoduliertes Signal und original Modulationsspannung.}
	\label{plt:teil_f}
\end{figure}

\subsection{Demodulation mit einer Gleichrichterdiode}
Die Spannung hinter einer Gleichrichterdiode ist in Abbildung \ref{plt:teil_g} oben dargestellt. Darunter wird das demodulierte Signal, welches nach dem Tiefpass gemessen wird, dargestellt.
Neben einem hochfrequenten überlagertem Rauschen erhält man die Modulationsspannung wieder zurück. Da der Modulationsgrad gering ist, ist die Spannung nicht sichtbar verzerrt.
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_g.PNG}
	\caption{Demodulation mit einer Gleichrichterdiode. Zeitabhängigkeit der Spannung hinter der Gleichrichterdiode oben und demoduliertes Signal unten.}
	\label{plt:teil_g}
\end{figure}

\subsection{Demodulation einer frequenzmodulierten Spannung}
In Abbildung \ref{plt:teil_h_u_c_2} ist die Spannung hinter dem Schwingkreis dargestellt. Diese entspricht dem amplitudenmodulierten Signal. Die Variation der Amplitude mit der Modualtionsfrequenz ist gut zu erkennen. Abbildung \ref{plt:teil_h_demoduliert} zeigt die demodulierte Spannung und die Modulationsspannung im Vergleich. Das demodulierte Signal enthält ein hochfrequentes Rauschen. Die ursprüngliche Frequenz kann jedoch identifiziert werden.

\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_h_demoduliert.PNG}
	\caption{Demodulation einer frequenzmodulierten Spannung. Demoduliertes Signal unten und original Modulationsspannung oben.}
	\label{plt:teil_h_demoduliert}
\end{figure}
\begin{figure}[tb]
	\centering
	\includegraphics[width=0.8\textwidth]{media/Teil_h_U_c_2.PNG}
	\caption{Amplitudenmoduliertes Signal nach dem Schwingreis.}
	\label{plt:teil_h_u_c_2}
\end{figure}

%------------------------------------------------
\section{Diskussion}
%------------------------------------------------
Bei der Amplitudenmodulation mit einem Ringmodulator sind die Frequenzbänder in der Fourieranalyse gut zu erkennen. Im Gegensatz zur Amplitudenmodulation mit Trägerabstrahlung ist hier kein Peak bei der Trägerfrequenz zu beobachten.

Die Demodulation mit einem zweiten Ringmodulator und gegebener Trägerspannung liefert ein klares Signal der Modulationsspannung zurück. Die Demodulation mit Hilfe einer Gleichrichterdiode liefert ein Signal mit größerem Rauschen.

Bei der Frequenzmodulation erkennt man die Verbreiterung des Trägerfrequenz-Peaks, aufgrund der Frequenzvariation um die Trägerfrequenz. Auch hier erhält man nach der Demodulation ein Signal mit hohem Rauschen. Bessere Ergebnisse könnten eventuell durch einen weiteren Tiefpass erreicht werden. Damit können  die hohen Frequenzen unterdrückt werden. Mit der richtigen Abstimmung bleibt das Signal erhalten und die hochfrequenten Störungen werden durch den Tiefpass herausgefiltert.

Die spektrale Auflösung der Frequenzanalyse des Oszilloskops beträgt $\Delta \nu = \frac{1}{T}$, wobei $T$ die Dauer eines Messintervalls ist, mit dem die schnelle Fouriertransformation durchgeführt wird. Das Messintervall ist einstellbar. Für die Amplitudenmodulation mit und ohne Trägerspannung wurden unterschiedliche Intervalle verwendet. In beiden Fällen können die Peaks jedoch unterschieden werden. Im ersten Fall ist die Auflösung \SI{2}{\giga\hertz} und im zweiten \SI{0.74}{\giga\hertz}.
Bei der Frequenzanalyse des frequenzmodulierten Signals wurde ein sehr kurzes Intervall von \SI{21}{\nano\second} gewählt, welches die Messung unbrauchbar macht, da damit eine Auflösung von \SI{48}{\giga\hertz} erreicht wird.

%------------------------------------------------
% LITERATURVERZEICHNIS
%------------------------------------------------
\begin{thebibliography}{xx}
	\small
	\bibitem{1}\enquote{Versuch \Versuchsnummer~- \Versuchstitel},\\
		http://129.217.224.2/HOMEPAGE/Anleitung\_FPMa.html,\\
		abgerufen am \Versuchsdatum

\end{thebibliography}

\end{document}
