# LaTeXOutput #

LaTeXOutput ist ein Python-Paket zur einfachen Erstellung von LaTeX-Tabellen direkt aus Python.

Eingebunden wird das Paket z.B. mit

	:::python

	import latexoutput as lout

## LaTeX-Tabellen ##

In Python verwendete Daten können ganz einfach in LaTeX-Tabellen exportiert werden. Der folgende Code-Schnipsel exportiert die Daten in den Arrays x und y in die Datei *tbl_x_y.tex* im *build* Ordner.

	:::python

	lout.latex_table(name="x_y", # definiert Dateiname und default-Label
	                 content=[x, y],
	                 col_title=["$x$-Daten", "$y$-Daten"], # optional, kann auch als kommagetrennter String übergeben werden
	                 col_unit=["\\meter", "\\milli\\meter"], # optional, kann auch als kommagetrennter String übergeben werden
	                 fmt=["3.1", "4.2"], # optional; 4.2 erzeugt: 4 Vorkomma-, 2 Nachkommastellen, kann auch als kommagetrennter String übergeben werden
	                 caption="Ein $x$-$y$-Plot.", # optional
	                 label="", # optional, default: "tbl:<name>"
	                 table_env=True, # optional, Steuert die Erzeugung einer table-Umgebung, default: True
	                 unpack=True) # optional, True, wenn content Liste von Arrays; False, wenn content 2D-Array; default: True

Die Tabelle kann dann mit ``\input{build/tbl_<name>}`` (bei Benutzung des Protokoll-Templates ``\tbl{<name>}``) in ein LaTeX-Dokument eingebunden werden.
Die Caption muss dabei im Python-Skript definiert werden.


## LaTeX-Werte ##

Auch berechnete Werte können einfach in LaTeX exportiert werden.
Der Vorteil ist, dass hierbei weniger Fehler als bei Copy+Paste entstehen können.
Außerdem wird der Wert im LaTeX-Dokument bei jeder Änderung der Berechnung im Skript direkt angepasst.

Für den Export der Variable *x* in die Datei *x.tex* im Ordner *build*:

	:::python

	lout.latex_val(name="x",
	               val=x,
	               unit="\\milli\\meter", # optional (nur bei use_siunitx=True)
	               digits=1, # optional, definiert die Anzahl der Nachkommastellen, auf die gerundet wird
	               use_siunitx=True, # optional, Benutzung des sinuitx-Pakets, default: True
	               siunitx_options="") # optional, übergibt dem siunitx-Paket zusätzliche Optionen (nur bei use_siunitx=True)

Der so gespeicherte Wert kann dann mit ``\input{build/val_<name>}`` (bei Benutzung des Protokoll-Templates ``\val{<name>}``) in ein LaTeX-Dokument eingebunden werden.
