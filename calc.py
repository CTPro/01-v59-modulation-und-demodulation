import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.stats import sem # standard error of mean
from uncertainties import ufloat, ufloat_fromstr
import uncertainties.unumpy as unp
from uncertainties.unumpy import (nominal_values as noms, std_devs as stds)
import scipy.constants as const
from latexoutput import latex_table, latex_val

print("INFO Started!\n")

dT, U, phi = np.genfromtxt("data/Teil_e.csv", unpack=True)

x = -np.cos(np.deg2rad(phi[5:-9]))
y = U[5:-9]

x_exc = np.append(-np.cos(np.deg2rad(phi[:5])), -np.cos(np.deg2rad(phi[-9:])))
y_exc = np.append(U[:5], U[-9:])

def f(x,a,b):
    return a*x + b

par, cov = curve_fit(f, x, y)
err = np.sqrt(np.diag(cov))

fit_a = ufloat(par[0],err[0])
fit_b = ufloat(par[1],err[1])
par_f = par

print("a: ", fit_a)
print("b: ", fit_b)

x_offset = (np.max(x)-np.min(x))*0.05
x_plot = np.linspace(np.min(x)-x_offset,np.max(x)+x_offset,100)
x_plot = np.linspace(-1.1, 1.1, 100)

fig = plt.figure()

# Einfacher Plot
plt.plot(x,y,'rx',label='Messdaten')
plt.plot(x_exc,y_exc,'x', color='0.5',label='unberücksichtigte Messdaten')

# Plot einer Ausgleichs- oder Theoriefunktion f
plt.plot(x_plot,f(x_plot,*par_f),'b-',label='Fit')

plt.xlabel("$\cos\phi$")
plt.ylabel("$U$ in mV")
plt.xlim(np.min(x_plot),np.max(x_plot))
plt.legend(loc='best')
plt.savefig("build/Teil_e.pdf")
plt.close()

print("\nINFO Finished!")
